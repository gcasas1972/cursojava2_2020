package ar.com.utn.ruleta.controller;

import java.sql.SQLException;
import java.util.List;

import ar.com.utn.ruleta.controller.opcionValidator.OpcionCRUDValidator;
import ar.com.utn.ruleta.dao.DAO;
import ar.com.utn.ruleta.modelo.Model;
import ar.com.utn.ruleta.modelo.Numero;
import ar.com.utn.ruleta.modelo.Opcion;
import ar.com.utn.ruleta.modelo.Opcion1Numero;
import ar.com.utn.ruleta.modelo.exceptions.RuletaException;

public class RuletaController implements Controller {

	public RuletaController() {	}

	@Override
	public void agregarHandler(Model pmodel, DAO pdao) throws RuletaException, ClassNotFoundException, SQLException {
	
		String strErroes = OpcionCRUDValidator.getErroresAlAgregar((Opcion)pmodel);
		if(!strErroes.isEmpty())
			throw new RuletaException(strErroes);
		else
			pdao.agregar(pmodel);
	}

	@Override
	public void modificarHandler(Model pmodel, DAO pdao) throws RuletaException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminarHandler(Model pmodel, DAO pdao) throws RuletaException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public List leernarHandler(Model pmodel, DAO pdao) throws RuletaException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
