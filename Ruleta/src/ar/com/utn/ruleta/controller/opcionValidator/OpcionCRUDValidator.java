package ar.com.utn.ruleta.controller.opcionValidator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ar.com.utn.ruleta.modelo.Jugador;
import ar.com.utn.ruleta.modelo.Opcion;

public abstract class OpcionCRUDValidator {
	protected static Opcion opcion;
	public OpcionCRUDValidator() {}
	//sigo la misma estructura de jugador
	public static String getErroresAlAgregar(Opcion pOpcion) throws ClassNotFoundException, SQLException {
		opcion=pOpcion;		
		//TODO Fabri y Fer con Ayuda de Las
		List<OpcionCRUDValidator> errores = new ArrayList<OpcionCRUDValidator>();
		errores.add(new SaldoNullOVacioOpcionValidator()); //TODO FABRI
		errores.add(new CodigoDebeSerCeroOpcionAgrValidator());//TODO FER
		errores.add(new ApuestaNulaValidator()); //TODO Lucas
		StringBuffer strErrors =new StringBuffer();
		for (OpcionCRUDValidator opcionCRUDValidator : errores) {
			if(opcionCRUDValidator.isError()) {
				strErrors.append(opcionCRUDValidator.getError());
				strErrors.append("\n");
			}
		}
		return strErrors.toString();
	}
	
	public static String getErroresAlModificar(Jugador pJugador) throws ClassNotFoundException, SQLException {
		return null;
		
	}
	
	public static String getErroresAlEliminar(Jugador pJugador) throws ClassNotFoundException, SQLException {
		return null;
	}
	
	/**
	 * Este m�todo sirve para que cada una de las clases hijas determise si se produce o no el error que tiene asignado
	 * 
	 * @return devuelve un true en el caso que el error asignado se cumple y false cuando dicho error no se cumple
	 * @throws ClassNotFoundException es un error de la base de datos y esta relacionado con no encontrar el driver que permite accedera a la base de datos.
	 * @throws SQLException corresponde a un error de sql 
	 */
	public abstract boolean isError() throws ClassNotFoundException, SQLException;
	
	/**
	 * Este metodo esta relacionado con la descripci�n del error que se cumple al momento de verificase que esta clase tiene el error asignado
	 * @return
	 */
	public abstract String getError();
	
	
}
