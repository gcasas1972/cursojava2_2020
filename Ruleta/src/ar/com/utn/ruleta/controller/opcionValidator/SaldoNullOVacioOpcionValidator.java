package ar.com.utn.ruleta.controller.opcionValidator;

import java.sql.SQLException;

import ar.com.utn.ruleta.modelo.Jugador;

public class SaldoNullOVacioOpcionValidator extends OpcionCRUDValidator{

	@Override
	public boolean isError() throws ClassNotFoundException, SQLException {						
		return opcion.getSaldo()==0;
				
	}

	@Override
	public String getError() {
		return "El saldo debe ser mayor a cero";
	}

	
}
