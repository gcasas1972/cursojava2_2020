package ar.com.utn.ruleta.controller.opcionValidator.test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.com.utn.ruleta.controller.opcionValidator.OpcionCRUDValidator;
import ar.com.utn.ruleta.modelo.Apuesta;
import ar.com.utn.ruleta.modelo.Numero;
import ar.com.utn.ruleta.modelo.Opcion;
import ar.com.utn.ruleta.modelo.Opcion1Numero;

public class OpcionCRUDValidatorTest {
//	Apuesta a = new Apuesta();
	Opcion1Numero b;
  //TODO Matias voy a necestiar una opcion1numero
	
	@Before
	public void setUp() throws Exception {
		b = new Opcion1Numero(100,new Numero(2),new Apuesta(null, new Numero(12),null));
		b.setCodigo(0);
	}

	@After
	public void tearDown() throws Exception {
		b=null;
	}

	@Test
	public void testSaldoCero() throws ClassNotFoundException, SQLException {
		b.setSaldo(0);
		assertTrue(OpcionCRUDValidator.getErroresAlAgregar(b).equals("El saldo debe ser mayor a cero\n"));
	}

	@Test
	public void testApuNull() throws ClassNotFoundException, SQLException {
		b.setApuesta(null);
		assertTrue(OpcionCRUDValidator.getErroresAlAgregar(b).equals("La apuesta es Nula\n"));
	}
	
	

}
