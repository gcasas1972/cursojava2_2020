package ar.com.utn.ruleta.controller.opcionValidator;

import java.sql.SQLException;

import ar.com.utn.ruleta.modelo.Opcion;
import ar.com.utn.ruleta.modelo.OpcionGrupo;

public class CodigoDebeSerCeroOpcionAgrValidator extends OpcionCRUDValidator{

	@Override
	public boolean isError() throws ClassNotFoundException, SQLException {
		return opcion.getCodigo()>0;
	}

	@Override
	public String getError() {
		return "El codigo debe ser cero";
	}

}
