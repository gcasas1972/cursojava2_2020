package ar.com.utn.ruleta.controller.opcionValidator;

import java.sql.SQLException;import ar.com.utn.ruleta.modelo.Numero;
import ar.com.utn.ruleta.modelo.Opcion;

public class ApuestaNulaValidator extends OpcionCRUDValidator {

	@Override
	public boolean isError() throws ClassNotFoundException, SQLException {
		return opcion.getApuesta() == null ||
				opcion.getApuesta().isVacio();
	}

	@Override
	public String getError() {
		return "La apuesta es Nula";
	}

}
