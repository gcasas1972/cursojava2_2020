package ar.com.utn.ruleta.modelo;

import java.util.ArrayList;
import java.util.List;

import ar.com.utn.ruleta.modelo.exceptions.RuletaException;

public class Apuesta implements Model {
	
	private int 			codigo								;
	private List<Opcion> 	opciones= new ArrayList<Opcion>()	;
	private Numero 			numeroGanador						;
	private Jugador 		jugador	 							;
	
	public Apuesta() {}

	public Apuesta(int codigo, List<Opcion> opciones, Numero numeroGanador, Jugador jugador) {
		super()								;
		
		this.codigo 		= codigo		;
		this.opciones 		= opciones		;
		this.numeroGanador 	= numeroGanador	;
		this.jugador 		= jugador		;
		
	}

	public Apuesta(List<Opcion> opciones, Numero numeroGanador, Jugador jugador) {
		super()								;
		
		this.opciones 		= opciones		;
		this.numeroGanador 	= numeroGanador	;
		this.jugador 		= jugador		;
	}

	public List<Opcion> getOpciones() {return opciones;}
	
	public void addOpcion(Opcion pOpcion) throws RuletaException{
		jugador.debitar(pOpcion.getSaldo());
		opciones.add(pOpcion);
	}
	
	public Numero getNumeroGanador() {return numeroGanador;}
	public void setNumeroGanador(Numero pNumeroGanador) {
		this.numeroGanador = pNumeroGanador;
		for (Opcion opcion : opciones) {
			if(opcion.validar(numeroGanador))
				jugador.acreditar(opcion.cobrar());
		}
	}
	
	public Jugador getJugador() {return jugador;}
	public void setJugador(Jugador jugador) {this.jugador = jugador;}

	public int getCodigo() {return codigo;}
	public void setCodigo(int codigo) {this.codigo = codigo;}

	@Override
	public boolean isVacio() {
		return codigo == 0 		&&
				opciones ==null &&
				jugador ==null 	&&
				numeroGanador ==null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		result = prime * result + ((jugador == null) ? 0 : jugador.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Apuesta other = (Apuesta) obj;
		if (codigo != other.codigo)
			return false;
		if (jugador == null) {
			if (other.jugador != null)
				return false;
		} else if (!jugador.equals(other.jugador))
			return false;
		return true;
	}
	
	
}
