package ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoComposite;

public class SelectOpcionGrupoSaldoComposite extends SelectOpcionGrupoComposite {

	public SelectOpcionGrupoSaldoComposite() {}

	@Override
	public boolean isMe() {
		return opg.getSaldo()>0;
	}

	@Override
	public String getWhereSql() {
		StringBuffer SQL = new StringBuffer("OPG_SALDO= ");
		SQL.append(opg.getSaldo());
		return SQL.toString();
	}

	

}
