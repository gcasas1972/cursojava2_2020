package ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoComposite;

import java.util.ArrayList;
import java.util.List;

import ar.com.utn.ruleta.modelo.OpcionGrupo;

public abstract class SelectOpcionGrupoComposite{

			
	protected static OpcionGrupo opg;
		
	public SelectOpcionGrupoComposite() {}
	

	public  static String getSqlTotal(OpcionGrupo pOpg){
		opg = pOpg;
		
		List<SelectOpcionGrupoComposite> campos= new ArrayList<SelectOpcionGrupoComposite>();

		campos.add(new SelectApuestaIdComposite());
		campos.add(new SelectOpcionGrupoSaldoComposite());
		campos.add(new SelectOpcionGrupoCodigoGrupoComposite());
		
		StringBuffer sql = new StringBuffer("SELECT OPG_ID, APU_ID, OPG_GRUPO, OPG_SALDO FROM RULETA.OPCION_GRUPO WHERE ");
		
		for (SelectOpcionGrupoComposite campo : campos) {
			if(campo.isMe()){
				sql.append(campo.getWhereSql());
				sql.append(" AND ");
		}
	}
		return  sql.toString().substring(0,sql.toString().length()-5).trim();
}
	

	public abstract boolean isMe();
	public abstract String getWhereSql();
}
