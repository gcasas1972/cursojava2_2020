package ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoStrategy;

import ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoComposite.SelectOpcionGrupoComposite;

public class SelectUnoVariosCamposOpcionGrupoStrategy extends SelectOpcionGrupoStrategy {

	public SelectUnoVariosCamposOpcionGrupoStrategy() {}

	@Override
	public String getSql() {		
		return SelectOpcionGrupoComposite.getSqlTotal(opcionGrupo);
	}

	@Override
	public boolean isMe() {
		return opcionGrupo.getSaldo()>0 ||
				opcionGrupo.getApuesta()!=null||
						opcionGrupo.getGrupoConst()>0;
	}

}
