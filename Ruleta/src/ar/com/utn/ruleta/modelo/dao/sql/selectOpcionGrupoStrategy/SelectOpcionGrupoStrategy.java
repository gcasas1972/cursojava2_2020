package ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoStrategy;

import java.util.ArrayList;
import java.util.List;

import ar.com.utn.ruleta.modelo.OpcionGrupo;

public abstract class SelectOpcionGrupoStrategy {


	public SelectOpcionGrupoStrategy() {}
	
	protected static OpcionGrupo opcionGrupo;
	
	public static SelectOpcionGrupoStrategy  getInstance(OpcionGrupo pOpG){
		opcionGrupo = pOpG;
		List<SelectOpcionGrupoStrategy> unoVariosStrategies = new ArrayList<SelectOpcionGrupoStrategy>();
		//aca estan todas las estrategias 
		
		unoVariosStrategies.add(new SelectVacioStrategy());
		unoVariosStrategies.add(new SelectIdOpcionGrupoStrategy());
		//Este es el que usa un CompoSite
		unoVariosStrategies.add(new SelectUnoVariosCamposOpcionGrupoStrategy());
		
		for (SelectOpcionGrupoStrategy selectVacioIdUnoVariosStrategy : unoVariosStrategies) 
			if(selectVacioIdUnoVariosStrategy.isMe())
				return selectVacioIdUnoVariosStrategy;
		
		return null;
	}

	public abstract  String getSql();
	public abstract boolean isMe();

	
}