package ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoComposite;


public class SelectOpcionGrupoCodigoGrupoComposite extends SelectOpcionGrupoComposite {

	public SelectOpcionGrupoCodigoGrupoComposite() {}

	@Override
	public boolean isMe() {
		return opg.getGrupoConst()>0;
	}

	@Override
	public String getWhereSql() {
		StringBuffer SQL = new StringBuffer("OPG_GRUPO= ");
		SQL.append(opg.getGrupoConst());
		return SQL.toString();
	}


}
