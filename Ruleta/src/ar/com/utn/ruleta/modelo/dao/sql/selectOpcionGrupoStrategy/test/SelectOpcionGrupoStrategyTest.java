package ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoStrategy.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.com.utn.ruleta.modelo.Apuesta;
import ar.com.utn.ruleta.modelo.Jugador;
import ar.com.utn.ruleta.modelo.Numero;
import ar.com.utn.ruleta.modelo.OpcionGrupo;
import ar.com.utn.ruleta.modelo.OpcionGrupoRojo;
import ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoStrategy.SelectOpcionGrupoStrategy;

public class SelectOpcionGrupoStrategyTest {
	OpcionGrupo opg;
	Apuesta apu;
	Jugador jug;

	@Before
	public void setUp() throws Exception {
		jug = new Jugador(1,"Prueba","Test","Pruebita123");
		apu = new Apuesta(1,null,new Numero(2),jug);
		opg = new OpcionGrupoRojo(200,apu);
	}

	@After
	public void tearDown() throws Exception {
	opg=null;
	apu=null;
	jug=null;
	}

	@Test
	public void testGetInstanceNulo() {
		assertEquals("SELECT OPG_ID, APU_ID, OPG_GRUPO, OPG_SALDO FROM RULETA.OPCION_GRUPO ",SelectOpcionGrupoStrategy.getInstance(null).getSql());
	}
	
	@Test
	public void testGetInstanceId() {
		opg.setCodigo(1);
		assertEquals("SELECT OPG_ID, APU_ID, OPG_GRUPO, OPG_SALDO FROM RULETA.OPCION_GRUPO WHERE OPG_ID = 1",SelectOpcionGrupoStrategy.getInstance(opg).getSql());
	}
	
	@Test
	public void testGetInstanceTodo() {
		assertEquals("SELECT OPG_ID, APU_ID, OPG_GRUPO, OPG_SALDO FROM RULETA.OPCION_GRUPO WHERE APU_ID= 1 AND OPG_SALDO= 200 AND OPG_GRUPO= 200",SelectOpcionGrupoStrategy.getInstance(opg).getSql());
	}
	
	@Test
	public void testGetInstanceSinApuesta() {
		opg.setApuesta(null);
		assertEquals("SELECT OPG_ID, APU_ID, OPG_GRUPO, OPG_SALDO FROM RULETA.OPCION_GRUPO WHERE OPG_SALDO= 200 AND OPG_GRUPO= 200",SelectOpcionGrupoStrategy.getInstance(opg).getSql());
	}
	
	@Test
	public void testGetInstanceSinApuestaNiSaldo() {
		opg.setApuesta(null);
		opg.setSaldo(0);
		assertEquals("SELECT OPG_ID, APU_ID, OPG_GRUPO, OPG_SALDO FROM RULETA.OPCION_GRUPO WHERE OPG_GRUPO= 200",SelectOpcionGrupoStrategy.getInstance(opg).getSql());
	}

	@Test
	public void testGetInstanceConApuestaConCodigoCero() {
		apu.setCodigo(0);
		assertEquals("SELECT OPG_ID, APU_ID, OPG_GRUPO, OPG_SALDO FROM RULETA.OPCION_GRUPO WHERE OPG_SALDO= 200 AND OPG_GRUPO= 200",SelectOpcionGrupoStrategy.getInstance(opg).getSql());
	}
}
