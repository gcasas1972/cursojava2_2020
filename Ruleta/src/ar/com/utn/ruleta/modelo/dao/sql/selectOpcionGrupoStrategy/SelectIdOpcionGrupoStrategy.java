package ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoStrategy;

public class SelectIdOpcionGrupoStrategy extends SelectOpcionGrupoStrategy {

	@Override
	public String getSql() {
		StringBuffer sql = new StringBuffer("SELECT OPG_ID, APU_ID, OPG_GRUPO, OPG_SALDO ");
							sql.append("FROM RULETA.OPCION_GRUPO ");
							sql.append("WHERE OPG_ID = ");
							sql.append(opcionGrupo.getCodigo());
		return sql.toString();
	}

	@Override
	public boolean isMe() {
		return opcionGrupo.getCodigo()>0;
	}

}
