package ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoComposite;


public class SelectApuestaIdComposite  extends SelectOpcionGrupoComposite{
	
		
	public SelectApuestaIdComposite() {}

	@Override
	public boolean isMe() {
		return opg.getApuesta() !=null 	&& 
			   opg.getApuesta().getCodigo()>0 ;
		}

	@Override
	public String getWhereSql() {
		StringBuffer SQL = new StringBuffer("APU_ID= ");
		SQL.append(opg.getApuesta().getCodigo());
		return SQL.toString();
	}


	
		
}
