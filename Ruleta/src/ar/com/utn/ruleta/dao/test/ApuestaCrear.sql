-- Agregar
INSERT INTO ruleta.jugadores (JUG_NOMBRE,JUG_APELLIDO,JUG_ALIAS) VALUES('Fernando_test','Diaz','asd')

-- Eliminar
INSERT INTO ruleta.jugadores (JUG_NOMBRE,JUG_APELLIDO,JUG_ALIAS) VALUES('Fabri_test','Coltro','asdasdf')
INSERT INTO ruleta.apuestas(JUG_ID,APU_NUMEROGANADOR) VALUES((select jug_id from ruleta.jugadores where jug_nombre='Fabri_test'),2)

-- Modificar
INSERT INTO ruleta.jugadores (JUG_NOMBRE,JUG_APELLIDO,JUG_ALIAS) VALUES('Lucas_test','Quintas','luquitas')
INSERT INTO ruleta.apuestas(JUG_ID,APU_NUMEROGANADOR) VALUES((select jug_id from ruleta.jugadores where jug_nombre='Lucas_test'),6)
INSERT INTO ruleta.opcion_grupo(OPG_GRUPO,APU_ID,OPG_SALDO) VALUES(100,(SELECT APU_ID FROM RULETA.APUESTAS WHERE JUG_ID = (SELECT JUG_ID FROM RULETA.JUGADORES WHERE JUG_ALIAS='luquitas')),200);
INSERT INTO ruleta.opcion_grupo(OPG_GRUPO,APU_ID,OPG_SALDO) VALUES(400,(SELECT APU_ID FROM RULETA.APUESTAS WHERE JUG_ID = (SELECT JUG_ID FROM RULETA.JUGADORES WHERE JUG_ALIAS='luquitas')),500);

-- Leer
INSERT INTO ruleta.jugadores (JUG_NOMBRE,JUG_APELLIDO,JUG_ALIAS) VALUES('Matias_test','Quintas','mati')
INSERT INTO ruleta.apuestas(JUG_ID,APU_NUMEROGANADOR) VALUES((select jug_id from ruleta.jugadores where jug_nombre='Matias_test'),8)
