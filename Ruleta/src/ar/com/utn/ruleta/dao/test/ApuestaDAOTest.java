package ar.com.utn.ruleta.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.ruleta.dao.ApuestaDAO;
import ar.com.utn.ruleta.dao.OpcionGrupoDAO;
import ar.com.utn.ruleta.modelo.Apuesta;
import ar.com.utn.ruleta.modelo.Jugador;
import ar.com.utn.ruleta.modelo.Numero;
import ar.com.utn.ruleta.modelo.Opcion;
import ar.com.utn.ruleta.modelo.OpcionGrupo;
import ar.com.utn.ruleta.modelo.dao.util.ConnectionManager;
import ar.com.utn.ruleta.modelo.exceptions.RuletaException;

public class ApuestaDAOTest {
	
	static Connection con;
	Statement stat;
	ResultSet rs;
	
	Jugador jugador1 = null;
	Apuesta apuesta = null;
	Jugador jugador2 = null;
	Apuesta apuesta2 = null;
	Jugador jugador3 = null;
	Apuesta apuesta3 = null;
	Jugador jugador4 = null;
	Apuesta apuesta4 = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//TODO Fabri y Fer
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( ApuestaDAOTest.class.getResource( "ApuestaCrear.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
	    Statement consulta= con.createStatement();

	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( ApuestaDAOTest.class.getResource( "ApuestaEliminar.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql ); // aca arma
	       }
	    }
	    
	    cm.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();
		
		ResultSet rs = stat.executeQuery("Select JUG_ID,JUG_NOMBRE,JUG_APELLIDO,JUG_ALIAS from ruleta.jugadores where JUG_NOMBRE = 'Fernando_test'");
		if (rs.next()){
			jugador1 = new Jugador(rs.getInt("JUG_ID"),rs.getString("JUG_NOMBRE"),rs.getString("JUG_APELLIDO"),rs.getString("JUG_ALIAS"));
		}
		
		/*rs = stat.executeQuery("SELECT APU_ID,APU_NUMEROGANADOR FROM ruleta.apuestas where JUG_ID =" + jugador.getCodigo());
		if (rs.next()){
			apuesta = new Apuesta(rs.getInt("APU_ID"), null,(new Numero(rs.getInt("APU_NUMEROGANADOR"))), jugador);
		}
		*/
		rs = stat.executeQuery("Select JUG_ID,JUG_NOMBRE,JUG_APELLIDO,JUG_ALIAS from ruleta.jugadores where JUG_NOMBRE = 'Fabri_test'");
		if (rs.next()){
			jugador2 = new Jugador(rs.getInt("JUG_ID"),rs.getString("JUG_NOMBRE"),rs.getString("JUG_APELLIDO"),rs.getString("JUG_ALIAS"));
		}
		
		rs = stat.executeQuery("SELECT APU_ID,APU_NUMEROGANADOR FROM ruleta.apuestas where JUG_ID =" + jugador2.getCodigo());
		if (rs.next()){
			apuesta2 = new Apuesta(rs.getInt("APU_ID"), null,(new Numero(rs.getInt("APU_NUMEROGANADOR"))), jugador2);
		}
		
		rs = stat.executeQuery("Select JUG_ID,JUG_NOMBRE,JUG_APELLIDO,JUG_ALIAS from ruleta.jugadores where JUG_NOMBRE = 'Lucas_test'");
		if (rs.next()){
			jugador3 = new Jugador(rs.getInt("JUG_ID"),rs.getString("JUG_NOMBRE"),rs.getString("JUG_APELLIDO"),rs.getString("JUG_ALIAS"));
		}
		
		List<Opcion> listOp= new ArrayList<Opcion>();
		rs = stat.executeQuery("SELECT OPG_ID, APU_ID, OPG_GRUPO, OPG_SALDO FROM ruleta.opcion_grupo WHERE "
				+ "APU_ID = (SELECT APU_ID FROM RULETA.APUESTAS WHERE JUG_ID = (SELECT JUG_ID FROM RULETA.JUGADORES WHERE JUG_ALIAS='luquitas'))");
		
		while(rs.next()){
			Opcion opcion = OpcionGrupo.getInstance(rs.getInt("OPG_GRUPO"));
			opcion.setSaldo(rs.getInt("OPG_SALDO"));
			listOp.add(opcion);
		
		}
		
		
		rs = stat.executeQuery("SELECT APU_ID,APU_NUMEROGANADOR FROM ruleta.apuestas where JUG_ID =" + jugador3.getCodigo());
		if (rs.next()){
			apuesta3 = new Apuesta(rs.getInt("APU_ID"), listOp, (new Numero(rs.getInt("APU_NUMEROGANADOR"))), jugador3);
		}
		
		rs = stat.executeQuery("Select JUG_ID,JUG_NOMBRE,JUG_APELLIDO,JUG_ALIAS from ruleta.jugadores where JUG_NOMBRE = 'Matias_test'");
		if (rs.next()){
			jugador4 = new Jugador(rs.getInt("JUG_ID"),rs.getString("JUG_NOMBRE"),rs.getString("JUG_APELLIDO"),rs.getString("JUG_ALIAS"));
		}
		
		rs = stat.executeQuery("SELECT APU_ID,APU_NUMEROGANADOR FROM ruleta.apuestas where JUG_ID =" + jugador4.getCodigo());
		if (rs.next()){
			apuesta4 = new Apuesta(rs.getInt("APU_ID"), null,(new Numero(rs.getInt("APU_NUMEROGANADOR"))), jugador4);
		}
		
		
		
	}

	@After
	public void tearDown() throws Exception {
		
		jugador1 = null;
		apuesta = null;
		jugador2 = null;
		apuesta2 = null;
		jugador3 = null;
		apuesta3 = null;
		con.close();
		
	}

	@Test
	public void testAgregar() {
		
		ApuestaDAO apu_agregar = new ApuestaDAO();
		
		try {
			
			apuesta = new Apuesta (null, new Numero (13), jugador1);
		
			apu_agregar.agregar(apuesta);
			
			StringBuffer sbSQL = new StringBuffer	("SELECT JUG_ID, APU_NUMEROGANADOR ");
			sbSQL.append							("FROM ruleta.apuestas where JUG_ID =");
			sbSQL.append							(jugador1.getCodigo());
			sbSQL.append							(" and APU_NUMEROGANADOR=13");
			
			ResultSet rs=stat.executeQuery(sbSQL.toString());
			rs.next();
			assertEquals(rs.getInt("JUG_ID"), jugador1.getCodigo());
				
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (RuletaException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testEliminar() {
		
		ApuestaDAO apu_eliminar = new ApuestaDAO();
		
		try {
			
		apu_eliminar.eliminar(apuesta2);
		
		
		StringBuffer sql = new StringBuffer("SELECT APU_ID,APU_NUMEROGANADOR FROM ruleta.apuestas where JUG_ID = ");
		sql.append(jugador2.getCodigo());		
		
		rs = stat.executeQuery(sql.toString());
		
		assertFalse(rs.next());
		
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testModificar() {
		
		ApuestaDAO apu_modi = new ApuestaDAO();
		
		try {
			
			apuesta3.setJugador(jugador1);
			apuesta3.setNumeroGanador(new Numero(24));
			apu_modi.modificar(apuesta3);
			
			StringBuffer sql = new StringBuffer("SELECT APU_ID, JUG_ID, APU_NUMEROGANADOR FROM ruleta.apuestas where JUG_ID= ");
			sql.append(jugador1.getCodigo());
			sql.append(" and APU_NUMEROGANADOR=24");
			
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();
			
			assertEquals(rs.getInt("APU_NUMEROGANADOR"), apuesta3.getNumeroGanador().getValor());
			
		} catch (RuletaException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testLeer() {
		
		ApuestaDAO apuDAO = new ApuestaDAO();
		
		try {
			List<Apuesta> apuestas = apuDAO.leer(apuesta4);
			assertEquals(apuesta4.getCodigo(), ((Apuesta) apuestas.get(0)).getCodigo());
			
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

}
