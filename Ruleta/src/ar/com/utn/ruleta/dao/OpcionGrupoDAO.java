package ar.com.utn.ruleta.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.com.utn.ruleta.modelo.Apuesta;
import ar.com.utn.ruleta.modelo.Jugador;
import ar.com.utn.ruleta.modelo.Opcion1Numero;
import ar.com.utn.ruleta.modelo.OpcionGrupo;
import ar.com.utn.ruleta.modelo.dao.sql.selectJugadorIdUnoVariosStrategy.SelectJugadorStrategy;
import ar.com.utn.ruleta.modelo.dao.sql.selectOpcionGrupoStrategy.SelectOpcionGrupoStrategy;
import ar.com.utn.ruleta.modelo.dao.util.ConnectionManager;
import ar.com.utn.ruleta.modelo.dao.util.SqlConstant;

public class OpcionGrupoDAO implements DAO {

	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<OpcionGrupo> opciones = new ArrayList<OpcionGrupo>();
		while (rs.next()){
			OpcionGrupo opcionGrupo =OpcionGrupo.getInstance(rs.getInt("OPG_GRUPO"));
		//	opcionGrupo.setApuesta(new Apuesta(rs.getInt("Apu_id"), opciones, numeroGanador, jugador));  y todas
			opcionGrupo.setSaldo(rs.getInt("OPG_SALDO"));
		//lo lleno todito
			opciones.add(opcionGrupo);			
	}
		return opciones;

}
	
	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		
		OpcionGrupo OP1 = (OpcionGrupo)obj; //pregunta 
		
		StringBuffer sbValores = new StringBuffer("INSERT INTO ruleta.opcion_grupo(OPG_GRUPO, APU_ID, OPG_SALDO) VALUES(");
		sbValores.append(OP1.getGrupoConst());
		sbValores.append(",");
		sbValores.append(OP1.getApuesta().getCodigo());
		sbValores.append(",");
		sbValores.append(OP1.getSaldo());		
		sbValores.append(");");
		agregar.executeUpdate(sbValores.toString());
		
		ConnectionManager.desconectar();
	

	}

	@Override
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		
		OpcionGrupo opg1 = (OpcionGrupo)obj;
		StringBuffer sbValores = new StringBuffer("DELETE FROM ruleta.opcion_grupo WHERE OPG_GRUPO= ");
		sbValores.append(opg1.getGrupoConst());
		sbValores.append(" AND APU_ID= ");
		sbValores.append(opg1.getApuesta().getCodigo());
				
		eliminar.executeUpdate(sbValores.toString());
		
		ConnectionManager.desconectar();
	
	}

	@Override
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		
		OpcionGrupo opg1 = (OpcionGrupo)obj;
		StringBuffer sbValores = new StringBuffer("UPDATE ruleta.opcion_grupo ");
		sbValores.append("SET opg_saldo = ");
		sbValores.append(opg1.getSaldo());
		sbValores.append(" WHERE OPG_GRUPO  =");
		sbValores.append(opg1.getGrupoConst());
		sbValores.append(" AND APU_ID=");
		sbValores.append(opg1.getApuesta().getCodigo());
				
		modificar.executeUpdate(sbValores.toString());
		
		ConnectionManager.desconectar();
	

	}

	@Override
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		OpcionGrupo opg1 = (OpcionGrupo)obj;
		ResultSet rs = leer.executeQuery(SelectOpcionGrupoStrategy.getInstance(opg1).getSql());	
		List opciones = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return opciones;
	}

}
